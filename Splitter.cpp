#include <string>
#include <vector>
#include <regex>


class Splitter
{
public:
    static std::vector<std::string>  Execute(const std::string & input_string)
    {
        std::regex space_regex(" ");
        std::sregex_token_iterator first{input_string.begin(), input_string.end(), space_regex, -1},last;
        return {first, last};
    }
};

