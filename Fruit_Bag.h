#include <string>


/*
* Fruit_Bag_Item - класс представляющий собой
* элемент рюкзака с фруктами, содержит следующие поля:
* _fruit_type - Поле типа enum, отвечающее за тип фрукта
* _fruit_weight - Поле типа double, отвечающее за вес фрукта
* _fruit_price - Поле типа double, отвечающее за стоимость фрукта
*/

class Fruit_Bag_Item
{
   std::string _fruit_type;
   double _fruit_weight;
   double _fruit_price;

   public:

   Fruit_Bag_Item(const std::string &);
   Fruit_Bag_Item(const std::string &,double,double);

   std::string GetFruitType() {return _fruit_type;}
   int GetFruitWeight() {return _fruit_weight;}
   double GetFruitPrice() {return _fruit_price;}

   friend std::ostream & operator << (std::ostream&,const Fruit_Bag_Item&);
};