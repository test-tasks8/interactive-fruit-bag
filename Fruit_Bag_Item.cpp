#include <ostream>

#include "Fruit_Bag_Item.h"
#include "Splitter.cpp"

Fruit_Bag_Item::Fruit_Bag_Item(const std::string& fruit_info)
{
    std::vector<std::string> fruit_info_array = Splitter::Execute(fruit_info);

    _fruit_type = fruit_info_array[0];
    _fruit_weight = std::stod(fruit_info_array[1]);
    _fruit_price = std::stod(fruit_info_array[2]);
}

Fruit_Bag_Item::Fruit_Bag_Item(const std::string& fruit_type,double fruit_weight,double fruit_price) :
                             _fruit_type(fruit_type), _fruit_weight(fruit_weight), _fruit_price(fruit_price) {}


std::ostream & operator<< (std::ostream& out_stream, const Fruit_Bag_Item & fruit_bag_item)
{
    out_stream << fruit_bag_item._fruit_type << " " << fruit_bag_item._fruit_weight << " " 
                << fruit_bag_item._fruit_price << std::endl;
    
    return out_stream;
}



